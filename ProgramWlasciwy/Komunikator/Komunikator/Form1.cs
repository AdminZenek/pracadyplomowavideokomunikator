﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace Komunikator
{
	public partial class Form1 : Form
	{

		public Form1()
		{
			InitializeComponent();

			var iWidth = 640; 
			var iHeight = 480;			

			pictureBoxMoj.Height = iHeight;
			pictureBoxMoj.Width = iWidth;

		}

		const short WM_CAP = 1024;
		const int WM_CAP_DRIVER_CONNECT = WM_CAP + 10;
		const int WM_CAP_DRIVER_DISCONNECT = WM_CAP + 11;
		const int WM_CAP_EDIT_COPY = WM_CAP + 30;
		const int WM_CAP_SET_PREVIEW = WM_CAP + 50;
		const int WM_CAP_SET_PREVIEWRATE = WM_CAP + 52;
		const int WM_CAP_SET_SCALE = WM_CAP + 53;
		const int WS_CHILD = 1073741824;
		const int WS_VISIBLE = 268435456;
		const short SWP_NOMOVE = 2;
		const short SWP_NOSIZE = 1;
		const short SWP_NOZORDER = 4;
		const short HWND_BOTTOM = 1;
		int hHwnd;
		int nrKamerki = 0;

		MemoryStream ms;
		BinaryWriter mysw;

		[System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SendMessageA")]
		static extern int SendMessage(int hwnd, int wMsg, int wParam, [MarshalAs(UnmanagedType.AsAny)] object lParam);
		[System.Runtime.InteropServices.DllImport("avicap32.dll")]
		static extern int capCreateCaptureWindowA(string lpszWindowName, int dwStyle, int x, int y, int nWidth, short nHeight, int hWndParent, int nID);
		[System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SetWindowPos")]
		static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);
		[System.Runtime.InteropServices.DllImport("user32")]
		static extern bool DestroyWindow(int hndw);


		private void buttonStart_Click(object sender, EventArgs e)
		{
			nrKamerki = int.Parse(numericUpDownnrKamery.Text);
			OpenPreviewWindow();
		}

		private void OpenPreviewWindow()
		{
			//  Open Preview window in picturebox
			// 
			hHwnd = capCreateCaptureWindowA(nrKamerki.ToString(), (WS_VISIBLE | WS_CHILD), 0, 0, pictureBoxMoj.Height,(short) pictureBoxMoj.Width, pictureBoxMoj.Handle.ToInt32(), 0);
			// 
			//  Connect to device
			// 
			if (SendMessage(hHwnd, WM_CAP_DRIVER_CONNECT, nrKamerki, 0) == 1)
			{
				// Set the preview scale
				SendMessage(hHwnd, WM_CAP_SET_SCALE, 1, 0);
				
				// Set the preview rate in milliseconds
				SendMessage(hHwnd, WM_CAP_SET_PREVIEWRATE, 66, 0);
				
				// Start previewing the image from the camera
				SendMessage(hHwnd, WM_CAP_SET_PREVIEW, 1, 0);
			
				//  Resize window to fit in picturebox
				SetWindowPos(hHwnd, HWND_BOTTOM, 0, 0, pictureBoxMoj.Width, pictureBoxMoj.Height , (SWP_NOMOVE | SWP_NOZORDER));



			}
			else
			{
				//  Error connecting to device close window
				DestroyWindow(hHwnd);
			}
		}


		void ZacznijNadawac()
		{

			ms = new MemoryStream();

			IDataObject data;
			Image bmap;

			//  Copy image to clipboard
			SendMessage(hHwnd, WM_CAP_EDIT_COPY, 0, 0);

			//  Get image from clipboard and convert it to a bitmap
			data = Clipboard.GetDataObject();

			if (data.GetDataPresent(typeof(System.Drawing.Bitmap)))
			{
				bmap = ((Image)(data.GetData(typeof(System.Drawing.Bitmap))));
				bmap.Save(ms, ImageFormat.Bmp);
			}

			pictureBoxMoj.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
			if (ms != null) pictureBoxModyfikowany.Image = Image.FromStream(ms);
			ms.Flush();
			ms.Close();
		}

		private void ClosePreviewWindow()
		{
			//  Disconnect from device			
			SendMessage(hHwnd, WM_CAP_DRIVER_DISCONNECT, nrKamerki, 0);

			//  close window
			DestroyWindow(hHwnd);
		}

		private void buttonStop_Click(object sender, EventArgs e)
		{
			ClosePreviewWindow();
		}


		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			ClosePreviewWindow();
		}


		int jeden = 1;
		private void timer1GetKolor_Tick(object sender, EventArgs e)
		{
			ZacznijNadawac();
			labelTestowy.Text = " >"+jeden.ToString()+"< kolor >";
			jeden++;
		}

		private void pictureBoxModyfikowany_Click(object sender, EventArgs e)
		{

		}
	}
}
