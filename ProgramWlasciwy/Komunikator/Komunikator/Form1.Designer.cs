﻿namespace Komunikator
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBoxMoj = new System.Windows.Forms.PictureBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.numericUpDownnrKamery = new System.Windows.Forms.NumericUpDown();
            this.buttonStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1GetKolor = new System.Windows.Forms.Timer(this.components);
            this.labelTestowy = new System.Windows.Forms.Label();
            this.pictureBoxModyfikowany = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMoj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownnrKamery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxModyfikowany)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxMoj
            // 
            this.pictureBoxMoj.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxMoj.Image")));
            this.pictureBoxMoj.ImageLocation = "";
            this.pictureBoxMoj.InitialImage = null;
            this.pictureBoxMoj.Location = new System.Drawing.Point(12, 52);
            this.pictureBoxMoj.Name = "pictureBoxMoj";
            this.pictureBoxMoj.Size = new System.Drawing.Size(640, 480);
            this.pictureBoxMoj.TabIndex = 0;
            this.pictureBoxMoj.TabStop = false;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(15, 9);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(99, 28);
            this.buttonStart.TabIndex = 1;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // numericUpDownnrKamery
            // 
            this.numericUpDownnrKamery.Location = new System.Drawing.Point(294, 12);
            this.numericUpDownnrKamery.Name = "numericUpDownnrKamery";
            this.numericUpDownnrKamery.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownnrKamery.TabIndex = 2;
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(120, 9);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(99, 28);
            this.buttonStop.TabIndex = 3;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(548, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Obraz Oryginalny";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(225, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Kamerka Nr";
            // 
            // timer1GetKolor
            // 
            this.timer1GetKolor.Enabled = true;
            this.timer1GetKolor.Interval = 1000;
            this.timer1GetKolor.Tick += new System.EventHandler(this.timer1GetKolor_Tick);
            // 
            // labelTestowy
            // 
            this.labelTestowy.AutoSize = true;
            this.labelTestowy.Location = new System.Drawing.Point(659, 52);
            this.labelTestowy.Name = "labelTestowy";
            this.labelTestowy.Size = new System.Drawing.Size(43, 13);
            this.labelTestowy.TabIndex = 6;
            this.labelTestowy.Text = "testowy";
            // 
            // pictureBoxModyfikowany
            // 
            this.pictureBoxModyfikowany.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxModyfikowany.Image")));
            this.pictureBoxModyfikowany.Location = new System.Drawing.Point(727, 52);
            this.pictureBoxModyfikowany.Name = "pictureBoxModyfikowany";
            this.pictureBoxModyfikowany.Size = new System.Drawing.Size(640, 480);
            this.pictureBoxModyfikowany.TabIndex = 7;
            this.pictureBoxModyfikowany.TabStop = false;
            this.pictureBoxModyfikowany.Click += new System.EventHandler(this.pictureBoxModyfikowany_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1379, 583);
            this.Controls.Add(this.pictureBoxModyfikowany);
            this.Controls.Add(this.labelTestowy);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.numericUpDownnrKamery);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.pictureBoxMoj);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMoj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownnrKamery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxModyfikowany)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxMoj;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.NumericUpDown numericUpDownnrKamery;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1GetKolor;
        private System.Windows.Forms.Label labelTestowy;
        private System.Windows.Forms.PictureBox pictureBoxModyfikowany;
    }
}

